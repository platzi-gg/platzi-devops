Summary

(Da el resumen del problema)

Steps to reproduce

(Los pasos para reproducirlo)

What is the current behavior?

(Da el el comportamiento actual)

What is the expected behavior?

(Da el comportamiento esperado)

